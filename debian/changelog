python-proliantutils (2.16.2-3) unstable; urgency=medium

  * Switch to pybuild (Closes: #1090589).

 -- Thomas Goirand <zigo@debian.org>  Wed, 18 Dec 2024 23:43:45 +0100

python-proliantutils (2.16.2-2) unstable; urgency=medium

  * Add fix-dependencies.patch.

 -- Thomas Goirand <zigo@debian.org>  Mon, 08 Apr 2024 09:30:38 +0200

python-proliantutils (2.16.2-1) unstable; urgency=medium

  * New upstream release.
  * Fixed (build-)depends for this release.

 -- Thomas Goirand <zigo@debian.org>  Sun, 07 Apr 2024 16:22:23 +0200

python-proliantutils (2.16.0-3) unstable; urgency=medium

  * Added python3-pyasyncore as (build-)depends (Closes: #1058145).

 -- Thomas Goirand <zigo@debian.org>  Tue, 12 Dec 2023 22:04:52 +0100

python-proliantutils (2.16.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Wed, 04 Oct 2023 22:18:19 +0200

python-proliantutils (2.16.0-1) experimental; urgency=medium

  * New upstream point release plus some commits.
  * Fixed (build-)depends for this release.
  * Cleans better.

 -- Thomas Goirand <zigo@debian.org>  Wed, 06 Sep 2023 08:51:23 +0200

python-proliantutils (2.14.0-3) unstable; urgency=medium

  * Cleans better (Closes: #1045286).

 -- Thomas Goirand <zigo@debian.org>  Tue, 15 Aug 2023 10:57:32 +0200

python-proliantutils (2.14.0-2) unstable; urgency=medium

  * Blacklist test_update_persistent_boot_uefi_target() that is broken under
    Python 3.11 (Closes: #1030469).

 -- Thomas Goirand <zigo@debian.org>  Tue, 14 Feb 2023 09:46:37 +0100

python-proliantutils (2.14.0-1) unstable; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Fri, 30 Sep 2022 09:50:35 +0200

python-proliantutils (2.13.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Thu, 24 Mar 2022 11:54:21 +0100

python-proliantutils (2.13.0-1) experimental; urgency=medium

  * New upstream release.
  * Fixed (build-)depends for this release.
  * Removed patches applied upstream.
  * Add autopkgtest.

 -- Thomas Goirand <zigo@debian.org>  Wed, 02 Mar 2022 15:35:34 +0100

python-proliantutils (2.12.0-1) unstable; urgency=medium

  [ Thomas Goirand ]
  * Uploading to unstable.
  * Add Fix_the_test_cases_for_latest_sushy.patch (Closes: #997545).

  [ Kevin Allioli ]
  * New upstream release.
  * Added myself in copyright and uploaders.

 -- Thomas Goirand <zigo@debian.org>  Fri, 12 Nov 2021 14:02:48 +0100

python-proliantutils (2.11.0-1) experimental; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Mon, 08 Mar 2021 12:54:01 +0100

python-proliantutils (2.10.0-2) unstable; urgency=medium

  * Uploading to unstable.
  * Add a debian/salsa-ci.yml.
  * Fixed watch file to use the git mode and the new upstream URL.

 -- Thomas Goirand <zigo@debian.org>  Wed, 14 Oct 2020 12:48:43 +0200

python-proliantutils (2.10.0-1) experimental; urgency=medium

  * New upstream release.
  * Fixed (build-)depends for this release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 09 Sep 2020 09:18:39 +0200

python-proliantutils (2.9.4-1) unstable; urgency=medium

  * New upstream release.
  * Fixed sushy version in (build-)depends.

 -- Thomas Goirand <zigo@debian.org>  Fri, 08 May 2020 18:39:41 +0200

python-proliantutils (2.9.2-1) unstable; urgency=medium

  [ Michal Arbet ]
  * New upstream version
  * Add me to maintainer field

  [ Thomas Goirand ]
  * Fix upstream VCS URLs in d/control, d/copyright and d/rules.
  * Blacklist failing unit tests: test_update_ilo_firmware() and
    test_update_other_component_firmware() (Closes: #950037).

 -- Thomas Goirand <zigo@debian.org>  Tue, 18 Feb 2020 09:49:22 +0100

python-proliantutils (2.8.2-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Sun, 20 Oct 2019 20:45:06 +0200

python-proliantutils (2.8.2-1) experimental; urgency=medium

  [ Ondřej Nový ]
  * Use debhelper-compat instead of debian/compat.

  [ Thomas Goirand ]
  * New upstream release.
  * Fixed (build-)depends for this release.

 -- Thomas Goirand <zigo@debian.org>  Wed, 18 Sep 2019 21:50:36 +0200

python-proliantutils (2.6.0-2) unstable; urgency=medium

  * Uploading to unstable:
    - Fixes FTBFS (Closes: #902241).

 -- Thomas Goirand <zigo@debian.org>  Thu, 30 Aug 2018 12:09:48 +0200

python-proliantutils (2.6.0-1) experimental; urgency=medium

  [ Ondřej Nový ]
  * Running wrap-and-sort -bast
  * d/control: Use team+openstack@tracker.debian.org as maintainer

  [ Thomas Goirand ]
  * New upstream release.
  * Removed remove-broken-test.patch.

 -- Thomas Goirand <zigo@debian.org>  Fri, 24 Aug 2018 13:58:09 +0200

python-proliantutils (2.5.0-3) unstable; urgency=medium

  * Disable broken test:
    test__get_socket_throws_exception_in_case_of_failed_connection.
  * Drop Python 2 module, only Python 3 is in use in Debian.

 -- Thomas Goirand <zigo@debian.org>  Mon, 30 Apr 2018 09:42:13 +0000

python-proliantutils (2.5.0-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Sun, 25 Feb 2018 22:56:35 +0000

python-proliantutils (2.5.0-1) experimental; urgency=medium

  [ Ondřej Nový ]
  * d/control: Set Vcs-* to salsa.debian.org

  [ Thomas Goirand ]
  * New upstream release.
  * Standards-Version is now 4.1.3.

 -- Thomas Goirand <zigo@debian.org>  Sun, 18 Feb 2018 20:35:54 +0000

python-proliantutils (2.4.1-2) unstable; urgency=medium

  * Uploading to unstable:
    - Fix FTBFS with Python 3.6 (Closes: #867629).
  * Removed some versions not needed since stretch release.

 -- Thomas Goirand <zigo@debian.org>  Sat, 28 Oct 2017 17:44:33 +0000

python-proliantutils (2.4.1-1) experimental; urgency=medium

  [ Ondřej Nový ]
  * Bumped debhelper compat version to 10

  [ Thomas Goirand ]
  * Updating vcs fields.
  * Updating copyright format url.
  * Running wrap-and-sort -bast.
  * Updating maintainer field.
  * Updating standards version to 4.1.1.
  * New upstream release.
  * Fixed (build-)depends for this release.
  * Blacklist test_successive_calls_to_extract_method().

 -- Thomas Goirand <zigo@debian.org>  Fri, 27 Oct 2017 20:02:37 +0200

python-proliantutils (2.1.11-2) unstable; urgency=medium

  * d/s/options: extend-diff-ignore of .gitreview
  * d/control: Using OpenStack's Gerrit as VCS URLs.

 -- Ondřej Nový <onovy@debian.org>  Mon, 26 Sep 2016 19:13:41 +0200

python-proliantutils (2.1.11-1) unstable; urgency=medium

  * New upstream release.

 -- Thomas Goirand <zigo@debian.org>  Tue, 20 Sep 2016 14:19:38 +0200

python-proliantutils (2.1.8-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * Fixed VCS URLs (https).
  * d/rules: Changed UPSTREAM_GIT protocol to https

  [ Thomas Goirand ]
  * New upstream release.
  * Fixed (build-)depends for this release (Closes: #827831).
  * Fixed debian/copyright ordering and holders.
  * Standards-Version is now 3.9.8 (no change).
  * Added Python 3 support.

 -- Thomas Goirand <zigo@debian.org>  Tue, 21 Jun 2016 17:48:19 +0200

python-proliantutils (2.1.5-1) unstable; urgency=medium

  * New upstream release.
  * Fixed VCS upstream URLs.

 -- Thomas Goirand <zigo@debian.org>  Thu, 22 Oct 2015 07:36:25 +0000

python-proliantutils (2.1.4-2) unstable; urgency=medium

  * Uploading to unstable.

 -- Thomas Goirand <zigo@debian.org>  Fri, 16 Oct 2015 10:28:15 +0000

python-proliantutils (2.1.4-1) experimental; urgency=medium

  * New upstream release.
  * Fixed (build-)depends for this release.
  * Delete python 3 patch.

 -- Thomas Goirand <zigo@debian.org>  Tue, 08 Sep 2015 19:38:36 +0000

python-proliantutils (2.1.0-1) unstable; urgency=medium

  * New upstream release.
  * Reviewed (build-)depends.
  * Using testr and subunit to run tests.
  * Removed python3 support since it's gone from upstream.

 -- Thomas Goirand <zigo@debian.org>  Tue, 28 Apr 2015 21:39:16 +0000

python-proliantutils (0.1.1-1) unstable; urgency=medium

  * Initial release. (Closes: #772019)

 -- Thomas Goirand <zigo@debian.org>  Thu, 04 Dec 2014 20:30:11 +0800
